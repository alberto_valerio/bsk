package tdd.training.bsk;

import java.util.ArrayList;
import java.util.List;

public class Game {
	
	public static final int MAX_FRAMES = 10;
	
	private List<Frame> frameSet;
	private int gameScore = 0;
	private int firstBonusScore = 0;
	private int secondBonusScore = 0;
	private int nStrikes = 0;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frameSet = new ArrayList<>(MAX_FRAMES);
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(frameSet.size() > MAX_FRAMES - 1) {
			throw new BowlingException("addFrame(Frame frame): Out of bound exception.");
		} else {
			frameSet.add(frame);			
		}
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if (index < 0 || index > MAX_FRAMES - 1) {
			throw new BowlingException("getFrameAt(int index): Out of bound exception.");
		}
		return frameSet.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		firstBonusScore = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		secondBonusScore = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusScore;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusScore;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		if (frameSet.size() != MAX_FRAMES) {
			throw new BowlingException("Game not completed yet!");
		} else {
			for (int i = 0; i < MAX_FRAMES; i++) {
				addSpareBonus(i);
				addStrikeBonus(i);
				gameScore += frameSet.get(i).getScore();				
			}			
		}
		return gameScore;
	}
	
	
	/**
	 * It checks if the frame is a spare and adds the consequent bonus points.
	 * 	 
	 * It consider the score of multiple spares and spare as last frame.
	 *
	 * @param index The in-range index of a Frame element.
	 */
	private void addSpareBonus(int index) {
		if (frameSet.get(index).isSpare()) {
			if (index != MAX_FRAMES - 1) {
				frameSet.get(index).setBonus(frameSet.get(index + 1).getFirstThrow());
			} else {
				gameScore += firstBonusScore;
			}
		}
	}

	
	/**
	 * It checks if the frame is a strike and adds the consequent bonus points.
	 * 
	 * It consider the score of multiple strikes, strike as last frame and The Perfect Game.
	 * 
	 * @param index The in-range index of a Frame element.
	 */
	private void addStrikeBonus(int index) {
		if (frameSet.get(index).isStrike()) {
			nStrikes++;
			
			if (index != MAX_FRAMES - 1) {
				
				if (frameSet.get(index + 1).isStrike() && index != MAX_FRAMES - 2) {
					frameSet.get(index).setBonus((frameSet.get(index + 1).getFirstThrow() + frameSet.get(index + 1).getSecondThrow() + frameSet.get(index + 2).getFirstThrow()));
					frameSet.get(index + 1).setBonus((frameSet.get(index + 2).getFirstThrow() + frameSet.get(index + 2).getSecondThrow()));						
				} else {
					frameSet.get(index).setBonus((frameSet.get(index + 1).getFirstThrow() + frameSet.get(index + 1).getSecondThrow()));										
				}
					
			} else {
				gameScore += secondBonusScore;
			}
			
			if (nStrikes == 9) {
				gameScore += firstBonusScore;
			}
			
		}
	}

	
}
