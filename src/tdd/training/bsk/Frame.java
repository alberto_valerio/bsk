package tdd.training.bsk;


public class Frame {
	
	private int firstThrowValue;
	private int secondThrowValue;
	
	private int bonusScore = 0;
	

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		if(firstThrow < 0 || firstThrow > 10) {
			throw new BowlingException("firstThrow has a value of: "+ firstThrow +". The value mast be between 0 and 10.");
		} else {
			firstThrowValue = firstThrow;			
		}
		if(secondThrow < 0 || secondThrow > 10) {
			throw new BowlingException("secondThrow has a value of: "+ secondThrow +". The value mast be between 0 and 10.");			
		} else {
			secondThrowValue = secondThrow;			
		}
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		return firstThrowValue;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		return secondThrowValue;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		bonusScore = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return bonusScore;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		return firstThrowValue + secondThrowValue + bonusScore;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		return firstThrowValue == 10 && secondThrowValue == 0;
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		return firstThrowValue + secondThrowValue == 10;
	}
	
	@Override
	public boolean equals(Object o) {
		Frame other = (Frame)o;
		return this.getFirstThrow() == other.getFirstThrow() && this.getSecondThrow() == other.getSecondThrow();
	}

}
