package bsk;

import static org.junit.Assert.*;


import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;


public class FrameTest {

	
	/** USER STORY #1 **/
	@Test
	public void testFrameFirstThrow() throws BowlingException{
		Frame frame = new Frame(2, 4);
		assertEquals(2, frame.getFirstThrow());
	}
	@Test
	public void testFrameSecondThrow() throws BowlingException{
		Frame frame = new Frame(2, 4);
		assertEquals(4, frame.getSecondThrow());
	}
	@Test (expected = BowlingException.class)
	public void testFrameFirstThrowOutOfRange() throws BowlingException{
		new Frame(35, 4);
	}
	@Test (expected = BowlingException.class)
	public void testFrameSecondThrowOutOfRange() throws BowlingException{
		new Frame(2, -7);
	}
	/** END USER STORY #1 **/

	
	/** USER STORY #2 **/
	@Test
	public void testFrameScore() throws BowlingException{
		Frame frame = new Frame(2, 6);
		assertEquals(8, frame.getScore());
	}
	/** END USER STORY #2 **/

	
	/** USER STORY #3 **/
	@Test
	public void testGetFrameAtFromGame() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));

		assertEquals(new Frame(3, 6), game.getFrameAt(1));
	}
	@Test (expected = BowlingException.class)
	public void testAddFrameToGameOutOfBoundCase() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));

		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
	}
	@Test (expected = BowlingException.class)
	public void testGetFrameAtFromGameOutOfBoundCase() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));

		assertEquals(new Frame(3, 6), game.getFrameAt(11));
	}
	/** END USER STORY #3 **/
	
	
	/** USER STORY #4 **/
	@Test
	public void testCalculateGameScore() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(81, game.calculateScore());
	}
	@Test (expected = BowlingException.class)
	public void testCalculateGameScoreFrameSetNotCompleted() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));

		game.calculateScore();
	}
	/** END USER STORY #4 **/
	
	
	/** USER STORY #5 **/
	@Test
	public void testCalculateGameScoreWithSpare() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(88, game.calculateScore());
	}
	@Test
	public void testCalculateGameScoreWithSpareOnLastFrame() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		
		assertEquals(90, game.calculateScore());
	}
	/** END USER STORY #5 **/
	
	
	/** USER STORY #6 **/
	@Test
	public void testCalculateGameScoreWithStrike() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(94, game.calculateScore());
	}
	/** END USER STORY #6 **/

	
	/** USER STORY #7 **/
	@Test
	public void testCalculateGameScoreStrikeFollowedBySpare() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(4, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(103, game.calculateScore());
	}
	/** END USER STORY #7 **/

	
	/** USER STORY #8 **/
	@Test
	public void testCalculateGameScoreMultipleStrikes() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(112, game.calculateScore());
	}
	/** END USER STORY #8 **/

	
	/** USER STORY #9 **/
	@Test
	public void testCalculateGameScoreMultipleSpares() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(98, game.calculateScore());
	}
	/** END USER STORY #9 **/

	
	/** USER STORY #10 **/
	@Test
	public void testCalculateGameScoreWithSpareAsLastFrame() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		
		game.setFirstBonusThrow(7);
		
		assertEquals(90, game.calculateScore());
	}
	/** END USER STORY #10 **/

	
	/** USER STORY #11 **/
	@Test
	public void testCalculateGameScoreWithStrikeAsLastFrame() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(92, game.calculateScore());
	}
	/** END USER STORY #11 **/

	
	/** USER STORY #12 **/
	@Test
	public void testCalculatePerfectGameScore() throws BowlingException{
		Game game = new Game();
		for (int i = 0; i < 10; i++) {
			game.addFrame(new Frame(10, 0));			
		}
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}
	/** END USER STORY #12 **/

}
